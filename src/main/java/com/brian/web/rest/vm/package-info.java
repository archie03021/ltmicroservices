/**
 * View Models used by Spring MVC REST controllers.
 */
package com.brian.web.rest.vm;
